var _ = require('underscore');

var messages = [
  {
    id: '1',
    content: 'I had a nice day on the beach today.'
  },
  {
    id: '2',
    content: 'Meeting Cara for a swim.'
  },
  {
    id: '3',
    content: 'Then have some BBQ dinner.'
  },
  {
    id: '4',
    content: 'Then more programming!'
  }
];
var id = 5;

var people = [
  {
    id: '8',
    name: 'Samson Ootoovak',
    username: 'ootoovak',
    messages: messages
  },
];

function get(collection, id) {
  return _.findWhere(collection, { id: id });
}

exports.name = 'fake_api';
exports.version = '0.0.0';
exports.register = function (plugin, options, next) {
  plugin.route({
    method: 'GET',
    path: '/api/people/{id}',
    handler: function (request, reply) {
      var found = get(people, request.params.id);
      reply(found).code(found ? 200 : 404);
    }
  });

  plugin.route({
    method: 'POST',
    path: '/api/messages',
    handler: function (request, reply) {
      var message = request.payload;
      message.id = id++;
      messages.push(message);
      reply(message).code(201);
    }
  });

  next();
};

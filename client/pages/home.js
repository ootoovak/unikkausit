var PageView    = require('./base');
var templates   = require('../templates');
var MessageView = require('../views/message');
var Messages    = require('../models/messages');

var ENTER_KEY = 13;

module.exports = PageView.extend({
  template: templates.pages.home,
  events: {
    'keypress [data-hook~=new-message]': 'handleNewMessage'
  },
  initialize: function() {
    this.model.fetch();
  },
  render: function () {
    this.renderWithTemplate();
    this.renderCollection(this.collection, MessageView, this.queryByHook('messages'));
  },
  handleNewMessage: function(event) {
    var newMessage = event.target;
    var value = newMessage.value.trim();
    if(event.which === ENTER_KEY && value) {
      this.collection.create({ content: value }, {
        wait: true,
        success: function () {
          newMessage.value = '';
        }
      });
    }
  }
});

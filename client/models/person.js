var AmpersandModel = require('ampersand-model');
var Messages    = require('../models/messages');

module.exports = AmpersandModel.extend({
  urlRoot: '/api/people/',
  props: {
    id: 'any',
    name: ['string', true, ''],
    username: ['string', true, '']
  },
  collections: {
    messages: Messages
  },
});

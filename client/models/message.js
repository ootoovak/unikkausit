var AmpModel = require('ampersand-model');

module.exports = AmpModel.extend({
  props: {
    id: 'any',
    name: ['string', true, ''],
    content: ['string', true, '']
  },
  derived: {
    avatar: {
      deps: ['firstName', 'lastName'],
      fn: function () {
        return 'http://robohash.org/' + encodeURIComponent(this.id) + '?size=80x80';
      }
    }
  }
});

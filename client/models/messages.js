var AmpCollection = require('ampersand-rest-collection');
var Message = require('./message');

module.exports = AmpCollection.extend({
  model: Message,
  url: '/api/messages'
});

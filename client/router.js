/*global me, app*/
var Router = require('ampersand-router');
var HomePage = require('./pages/home');
var Person = require('./models/person')

module.exports = Router.extend({
  routes: {
    '': 'home'
  },
  home: function () {
    this.trigger('page', new HomePage({
      model: me,
      collection: me.messages
    }));
  },
  catchAll: function () {
    this.redirectTo('');
  }
});

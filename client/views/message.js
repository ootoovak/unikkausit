var View = require('ampersand-view');
var templates = require('../templates');

module.exports = View.extend({
  template: templates.includes.message,
  bindings: {
    'model.avatar': {
      type: 'attribute',
      hook: 'avatar',
      name: 'src'
    },
    'model.name': '[data-hook~=name]',
    'model.content': '[data-hook~=content]'
  }
});
